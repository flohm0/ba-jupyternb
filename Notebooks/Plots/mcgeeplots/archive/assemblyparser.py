import jsonlines
import pandas as pd

json_file="/home/floh/full_bioproject_assembly/ncbi_dataset/assembly_data_report.jsonl"
csv_file="/home/floh/full_bioproject_assembly/grates.csv"

df = pd.read_csv(csv_file, usecols=['Name', 'Assemblyname'])

def addassembly_json(df, json_path):
    assemblystats=[[],[]]
    i=0
    with jsonlines.open(json_path, mode='r') as file:
        for assembly in file:
            assemblystats[0].append(assembly['accession'])
            assemblystats[1].append(assembly['assemblyInfo']['biosample']['attributes'][7]['value'])
            i+=1
    i=0
    for item in assemblystats[0]:
        df.loc[ df['Assemblyname'] == item , ['Name']] = assemblystats[1][i]
        i+=1
    return df
addassembly_json(df, json_file)
print(df)